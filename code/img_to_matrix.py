# Math library
import matplotlib.image as img
import matplotlib.pyplot as plt


# Function that converts the image into a binary matrix
def img_to_matrix(path):
    # Opening the image
    image = img.imread(path)

    # Return a simple matrix
    return image[:, :, 0]


# Function that can be used to plot an image
def plot_img(path):
    # Opening the image
    image = img.imread(path)

    # Plotting the picture of the letter
    plt.imshow(image)
    plt.show()


'''
# Going throw the matrix
for pix in image:
    print(pix)
'''
