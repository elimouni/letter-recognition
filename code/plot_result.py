# Import the libraries
import numpy as np
import matplotlib.image as img
import matplotlib.pyplot as plt


# If you want to plot the picture and the center of gravity, use this function
def plot_the_result(matrix, cog):
    plt.imshow(matrix, cmap='binary')
    plt.scatter(cog[1], cog[0], color='b')
    plt.show()
