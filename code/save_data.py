# CSV
import pandas as pd
from openpyxl import Workbook
from openpyxl import load_workbook


# Function to save the data of a letter in the XLSX file
def save_letter(information):
    # Path to the file
    filename = "data/letters.xlsx"

    # Prepare the new row to insert
    new_row = [information[0], information[1], information[2], information[3], information[4], information[5], information[6]]

    # Confirm file exists.
    # If not, create it, add headers, then append new data
    try:
        # Try to load the workbook
        wb = load_workbook(filename)
        # Select first worksheet
        ws = wb.worksheets[0]
    # If the file doesn't exist
    except FileNotFoundError:
        # Set the headers of the data
        headers_row = ['Letter', 'grav_x', 'grav_y', 'a', 'r^2', 'var_x', 'var_y']
        wb = Workbook()
        ws = wb.active
        # Insert the header
        ws.append(headers_row)

    # Insert a new row and save the file
    ws.append(new_row)
    wb.save(filename)


# Function to read the data that is in the XLSX file
def read_data():
    try:
        # Read the XLSX file and put the content in a variable
        reader = pd.read_excel(r'data/letters.xlsx')
    except FileNotFoundError:
        # If the file doesn't exist then return 0
        return 0
    else:
        # Else return the content of the file
        return reader
