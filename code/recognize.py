import img_to_matrix as imt
import geometric as cog
import plot_result as prs
import save_data as sdt
import train as trn
import numpy as np


# Function to recognize a letter written on a picture and to return it as a char
def letter_recognition(path):
    global letter
    first = True
    error = list()

    # Use the data algorithm to get the data of the Letter to analyze
    m_x, m_y, a, r2, var_x, var_y = trn.get_letter_data(path)

    # Open the XLSX file that contains all the data generated during trainings
    reader = sdt.read_data()
    dataset = np.array(reader)

    # Copy the useful data in an array
    for row in dataset:
        temp_error = list()

        temp_error.append(row[0])
        temp_error.append(abs((row[4] - r2) / r2) * 100)
        temp_error.append(abs((row[5] - var_x) / var_x) * 100)
        temp_error.append(abs((row[6] - var_y) / var_y) * 100)

        error.append(temp_error)

    min_error = 0

    # Set the coefficient for x variance, y variance and r2
    c_r2 = 0.8
    c_var_x = 0.1
    c_var_y = 0.1

    # for each line, compare it to the data of the analyzed letter to find the letter
    for row in error:
        temp = row[1]*c_r2 + row[2]*c_var_x + row[3]*c_var_y
        if first:
            min_error = temp
            letter = row[0]
            first = False
        else:
            if min_error > temp:
                min_error = temp
                letter = row[0]

    return letter
