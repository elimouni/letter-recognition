# import libs
import train as trn
import recognize as rcz

# To train the algorithm, load your dataset using [letter, path_to_the_image]
data = [["C", "../data/C_arial_36.png"], ["B", "../data/B_arial_36.png"], ["F", "../data/F_arial_36.png"], ["I", "../data/I_arial_36.png"]]

# Launch the training
trn.launch_training(data)

# Launch the recognition using the path to your picture as a parameter
letter = rcz.letter_recognition("test/lettre_mystere1.png")
print("The letter may be a", letter, "!")
