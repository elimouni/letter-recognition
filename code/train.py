# Import personal libraries and usual ones to make all the tasks
import img_to_matrix as imt
import geometric as cog
import plot_result as prs
import save_data as sdt


# Function that is used to train the algorithm using a dataset in parameter
# The parameter has this format : [letter, path_to_the_image]
def launch_training(letters):
    for letter in letters:
        # Path to the image file
        path = letter[1]

        # Get the data of the letter
        m_x, m_y, a, r2, var_x, var_y = get_letter_data(path)

        # Save it in the XLSX file
        sdt.save_letter([letter[0], m_x, m_y, a, r2, var_x, var_y])


def get_letter_data(path):
    # Transform the image in a matrix
    binary_matrix = imt.img_to_matrix(path)

    # Get the coordinates of the center of gravity of the matrix
    center_of_gravity = cog.center_mass(binary_matrix)

    # Projection on x and on y
    sum_x = cog.center_sum_x(binary_matrix)
    sum_y = cog.center_sum_y(binary_matrix)

    # Calculating ARL using least_squares
    a, r2 = cog.arl_least_squares(binary_matrix)
    var_x, var_y = cog.variances_profils(binary_matrix)

    return center_of_gravity[0], center_of_gravity[1], a, r2, var_x, var_y
