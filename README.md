# Simple Letter Recognition project

## Installation

Start by cloning the repository. You might have to install other dependancies to use the project for mathematical operations or files editing.
To do that, us the pip command :

```bash
pip install ModuleName
```

The needed modules are :
- matplotlib
- pandas
- scipy
- scikit-learn
- xlsxwriter
- xlrd
- openpyxl

## How to use the project

To start recognizing letters, you will have to train the algorithm before.

### Training

Use the function "launch_training" using your dataset, using the format described in the main.py file.

A XLSX file will be generated. It contains the data made using the dataset with the training function.

### Recognition

Use the function "letter_recognition" using the path to the image you want to find a letter in, as described in the setup.py file.
